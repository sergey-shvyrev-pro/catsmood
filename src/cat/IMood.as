/**
 * Created by sergey on 16.03.14.
 */
package cat {
import cat.Cat;

public interface IMood {
    function showPlayReaction():void;
    function showFeedReaction():void;
    function showStrokeReaction():void;
    function showKickReaction():void;
    function toSource():Object
    function toString():String;
}
}
