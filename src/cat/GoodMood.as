/**
 * Created by sergey on 16.03.14.
 */
package cat {
public class GoodMood implements IMood {
    private var _cat:Cat;
    private var _reactions:ReactionsCreator;

    public function GoodMood(cat:Cat, reactions:ReactionsCreator) {
        this._cat = cat;
        this._reactions = reactions;
    }

    public function showPlayReaction():void {
        this._cat.changeView(this._reactions.create("good").toPlaying);
        this._cat.changeMood(this._cat.badMood);
    }

    public function showFeedReaction():void {
        this._cat.changeView(this._reactions.create("good").toFeeding);
        this._cat.changeMood(this._cat.greatMood);
    }

    public function showStrokeReaction():void {
        this._cat.changeView(this._reactions.create("good").toStroking);
        this._cat.changeMood(this._cat.greatMood);
    }

    public function showKickReaction():void {
        this._cat.changeView(this._reactions.create("good").toKicking);
        this._cat.changeMood(this._cat.badMood);
    }

    public function toSource():Object {
        return {text: "Хорошее", color: 0x00ff00};
    }

    public function toString():String {
        return "good";
    }
}
}