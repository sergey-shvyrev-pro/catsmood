/**
 * Created by sergey on 16.03.14.
 */
package cat {
public class BadMood implements IMood {
    private var _cat:Cat;
    private var _reactions:ReactionsCreator;
    public function BadMood(cat:Cat, reactions:ReactionsCreator) {
        this._cat = cat;
        this._reactions = reactions;
    }

    public function showPlayReaction():void {
        this._cat.changeView(this._reactions.create("bad").toPlaying);
    }

    public function showFeedReaction():void {
        this._cat.changeView(this._reactions.create("bad").toFeeding);
        this._cat.changeMood(this._cat.goodMood)
    }

    public function showStrokeReaction():void {
        this._cat.changeView(this._reactions.create("bad").toStroking);
    }

    public function showKickReaction():void {
        this._cat.changeView(this._reactions.create("bad").toKicking);
        // вот тут я не понял, у нее же сейчас плохое настроение
        this._cat.changeMood(this._cat.badMood);
    }

    public function toSource():Object {
        return {text: "Плохое", color: 0xff0000};
    }

    public function toString():String {
        return "bad";
    }
}
}
