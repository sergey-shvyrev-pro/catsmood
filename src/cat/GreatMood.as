/**
 * Created by sergey on 16.03.14.
 */
package cat {
public class GreatMood implements IMood {
    private var _cat:Cat;
    private var _reactions:ReactionsCreator;
    public function GreatMood(cat:Cat, reactions:ReactionsCreator) {
        this._cat = cat;
        this._reactions = reactions;
    }

    public function showPlayReaction():void {
        this._cat.changeView(this._reactions.create("great").toPlaying);
        this._cat.changeMood(this._cat.goodMood)
    }

    public function showFeedReaction():void {
        this._cat.changeView(this._reactions.create("great").toFeeding);
    }

    public function showStrokeReaction():void {
        this._cat.changeView(this._reactions.create("great").toStroking);
    }

    public function showKickReaction():void {
        this._cat.changeView(this._reactions.create("great").toKicking);
        this._cat.changeMood(this._cat.badMood);
    }

    public function toSource():Object {
        return {text: "Отличное", color: 0xfff};
    }

    public function toString():String {
        return "great";
    }
}
}
