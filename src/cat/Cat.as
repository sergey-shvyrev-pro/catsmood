/**
 * Created by sergey on 16.03.14.
 */
package cat {
import events.CatEvent;

import flash.events.EventDispatcher;

import reactions.CatReactions;

public class Cat extends EventDispatcher {
    private var _goodMood:IMood;
    private var _badMood:IMood;
    private var _greatMood:IMood;
    private var _currentMood:IMood;
    private var _reactions:CatReactions;
    private var _currentView:String;
    public function Cat() {
        this._reactions = new CatReactions();

        this._goodMood= new GoodMood(this, this._reactions);
        this._badMood= new BadMood(this, this._reactions);
        this._greatMood = new GreatMood(this, this._reactions);
    }

    public function changeMood(mood:IMood):void {
        this._currentMood = mood;
        super.dispatchEvent(new CatEvent(CatEvent.CHANGE_MOOD));
    }

    public function play():void {
        this._currentMood.showPlayReaction();
    }

    public function feed():void {
        this._currentMood.showFeedReaction();
    }

    public function stroke():void {
        this._currentMood.showStrokeReaction();
    }

    public function kick():void {
        this._currentMood.showKickReaction();
    }

    public function get greatMood():IMood {
        return _greatMood;
    }

    public function get badMood():IMood {
        return _badMood;
    }

    public function get goodMood():IMood {
        return _goodMood;
    }

    public function get currentMood():IMood {
        return _currentMood;
    }

    public function changeView(view:String):void {
        this._currentView = view;
        super.dispatchEvent(new CatEvent(CatEvent.SHOW_REACTION));
    }

    public function get currentView():String {
        return _currentView;
    }
}
}
