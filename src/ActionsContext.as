/**
 * Created by sergey on 16.03.14.
 */
package {
import actions.IAction;

import cat.Cat;

public class ActionsContext {
    protected var action:IAction;
    public function ActionsContext() {
    }

    public function makeAction(recepient:*):void {
        action.make(recepient);
    }
}
}
