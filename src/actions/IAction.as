/**
 * Created by sergey on 15.03.14.
 */
package actions {
public interface IAction {
    function make(recepient:*):void;
}
}
