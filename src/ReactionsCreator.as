/**
 * Created by sergey on 15.03.14.
 */
package {
import reactions.*;

import flash.errors.IllegalOperationError;
import flash.utils.getQualifiedClassName;

public class ReactionsCreator {

    public function ReactionsCreator() {
    }

    public function create(type:String):IReactions {
        throw new IllegalOperationError("Method must be overridden in " + getQualifiedClassName(this));
        return null;
    }
}
}
