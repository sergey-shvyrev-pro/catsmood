/**
 * Created by sergey on 15.03.14.
 */
package reactions {
public interface IDisposable {
    function dispose():void;
}
}
