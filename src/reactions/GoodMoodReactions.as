/**
 * Created by sergey on 15.03.14.
 */
package reactions {

public class GoodMoodReactions implements IReactions, IDisposable {
    public function GoodMoodReactions() {
    }

    public function get toPlaying():String {
        return "Медленно бегает за мячиком";
    }

    public function get toFeeding():String {
        return "Быстро все съедает";
    }

    public function get toStroking():String {
        return "Мурлычет";
    }

    public function get toKicking():String {
        return "Убегает на ковер и писает";
    }

    public function dispose():void {
    }
}
}
