/**
 * Created by sergey on 15.03.14.
 */
package reactions {
import flash.utils.Dictionary;

public class CatReactions extends ReactionsCreator {

    public function CatReactions() {
        super();
    }

    override public function create(type:String):IReactions {

        var result:IReactions;

        switch (type){
            case "good":
                    result = new GoodMoodReactions();
                break;
            case "great":
                    result = new GreatMoodReactions();
                break;
            case "bad":
                    result = new BadMoodReactions();
                break;
            default :
                    throw new Error("No reactions for this mood type");
                break;
        }

        return result;
    }
}
}
