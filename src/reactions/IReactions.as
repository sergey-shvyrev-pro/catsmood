/**
 * Created by sergey on 15.03.14.
 */
package reactions {
import flash.display.MovieClip;

public interface IReactions {
    function get toPlaying():String;
    function get toFeeding():String;
    function get toStroking():String;
    function get toKicking():String;
}
}
