/**
 * Created by sergey on 15.03.14.
 */
package reactions {

public class BadMoodReactions implements IDisposable, IReactions {
    public function BadMoodReactions() {
    }

    public function dispose():void {
    }

    public function get toPlaying():String {
        return "Сидит на месте";
    }

    public function get toFeeding():String {
        return "Все съедает, но если в это время подойти - поцарапает";
    }

    public function get toStroking():String {
        return "Царапает";
    }

    public function get toKicking():String {
        return "Прыгает и кусает за правое ухо";
    }
}
}
