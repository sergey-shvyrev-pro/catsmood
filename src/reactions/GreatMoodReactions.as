/**
 * Created by sergey on 15.03.14.
 */
package reactions {

public class GreatMoodReactions implements IDisposable, IReactions {
    public function GreatMoodReactions() {
    }

    public function dispose():void {
    }

    public function get toPlaying():String {
        return "Носится, как угорелая";
    }

    public function get toFeeding():String {
        return "Быстро все съедает";
    }

    public function get toStroking():String {
        return "Мурлычет и виляет хвостом";
    }

    public function get toKicking():String {
        return "Убегает в другую комнату";
    }
}
}
