/**
 * Created by sergey on 16.03.14.
 */
package events {
import flash.events.Event;

public class CatEvent extends Event {
    public static const CHANGE_MOOD:String = "change_mood";
    public static const SHOW_REACTION:String = "show_reaction";
    public function CatEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
    }
}
}
